echo "this emulates a startup file in the container by running a dir command and directing the result to a timestamped logfile on the host volume share"
set d=%DATE:~-4%-%DATE:~4,2%-%DATE:~7,2%
set t=%time::=.% 
set t=%t: =%
set logfile="%d%%t%.log"
dir > c:\mydata\log%logfile%.txt
